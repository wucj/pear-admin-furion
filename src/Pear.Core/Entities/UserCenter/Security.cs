﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pear.Core
{
    /// <summary>
    /// 权限表
    /// </summary>
    public class Security : Entity, IEntityTypeBuilder<Security>
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public Security()
        {
            CreatedTime = DateTimeOffset.Now;
            IsDeleted = false;
            Enabled = true;
        }


        /// <summary>
        /// 菜单名称
        /// </summary>
        [Required, MaxLength(32)]
        public string Name { get; set; }
        /// <summary>
        /// 菜单图标
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 菜单地址
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 菜单打开方式
        /// </summary>
        public string Target { get; set; }
        /// <summary>
        /// 菜单类型
        /// </summary>
        public SecurityType? Type { get; set; }
        /// <summary>
        /// 权限标识
        /// </summary>
        public string Authorize { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [MaxLength(200)]
        public string Remark { get; set; }

        /// <summary>
        /// 序号/排序
        /// </summary>
        [Required]
        public int Sequence { get; set; }


        /// <summary>
        /// 是否启用
        /// </summary>
        public bool Enabled { get; set; }


        /// <summary>
        /// 所属菜单Id
        /// </summary>
        public int? ParentId { get; set; }

        /// <summary>
        /// 所属菜单
        /// </summary>
        public Security Parent { get; set; }


        /// <summary>
        /// 次级列表
        /// </summary>
        public ICollection<Security> Sublevels { get; set; }

        /// <summary>
        /// 多对多
        /// </summary>
        public ICollection<Role> Roles { get; set; }

        /// <summary>
        /// 多对多中间表
        /// </summary>
        public List<RoleSecurity> RoleSecurities { get; set; }




        /// <summary>
        /// 配置实体
        /// </summary>
        /// <param name="entityBuilder"></param>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        public void Configure(EntityTypeBuilder<Security> entityBuilder, DbContext dbContext, Type dbContextLocator)
        {
            entityBuilder
                .HasMany(x => x.Sublevels)
                .WithOne(x => x.Parent)
                .HasForeignKey(x => x.ParentId);

        }


    }
}