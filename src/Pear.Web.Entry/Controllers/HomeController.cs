﻿
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Pear.Web.Entry.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
      

        public HomeController()
        {
      
        }

        public IActionResult Index()
        {
            return View();
        }




        /// <summary>
        /// 异常视图
        /// </summary>
        /// <returns></returns>
        public IActionResult Exception(string message)
        {
            return View(message);
        }

        public IActionResult Error403()
        {
            return View();
        }

        public IActionResult Error404()
        {
            return View();
        }

        public IActionResult Error500()
        {
            return View();
        }

    }
}