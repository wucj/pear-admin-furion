﻿using Furion;
using Furion.DatabaseAccessor;
using Furion.UnifyResult;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Pear.Core;
using Pear.Web.Core;
using System;

namespace Pear.Web.Entry.Areas.Admin.Controllers
{

    /// <summary>
    /// 后台控制器
    /// </summary>
    [Area("Admin")]
    [Route("Admin/[controller]/[action]")]
    [AppAuthorize]
    public class HomeController : BaseController
    {

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="userRepository"></param>
        public HomeController(IRepository<User> userRepository)
            :base(userRepository)
        {

        }

        /// <summary>
        /// 模块后台主页面视图
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            ViewBag.User = User;
            ViewBag.UserId = UserId;
            return View();
        }

        /// <summary>
        /// 模块设置视图
        /// </summary>
        /// <returns></returns>
        public IActionResult Config()
        {
            return View();
        }


        /// <summary>
        /// 控制台视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.home.console:view")]
        public IActionResult Console()
        {
            return View();
        }


        /// <summary>
        /// 服务器信息视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.home.server:view")]
        public IActionResult Server()
        {
            return View();
        }

        /// <summary>
        /// 获取服务器信息
        /// </summary>
        /// <returns></returns>
        public IActionResult GetServer()
        {
            var systemUtil = App.GetService<SystemUtil>();
            return Json(new RESTfulResult<object>
            {
                StatusCode = 200,
                Succeeded = true,
                Data = new
                {
                    CPURate = systemUtil.GetCPURate(),
                    RAMRate = systemUtil.GetRAMRate(),
                    TotalRAM = systemUtil.GetTotalRAM(),
                    DotNetRAM = systemUtil.GetDotNetRAM(),
                    StartupTime = systemUtil.GetStartupTime(),
                    RunTime = systemUtil.GetRunTime()
                },
                Extras = UnifyContext.Take(),
                Timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds()
            });
        }

    }
}
