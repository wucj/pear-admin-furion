﻿using Furion.DependencyInjection;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pear.Web.Core
{
    /// <summary>
    /// Session 操作类
    /// </summary>
    public class SessionUtil : IScoped
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public SessionUtil(IHttpContextAccessor httpContextAccessor)
        {
            this._httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// 新增复杂型session abelp 2020/12/08 14:54
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetObject<T>(string key, T value)
        {
            _httpContextAccessor.HttpContext.Session.SetString(key, JsonConvert.SerializeObject(value));
        }

        /// <summary>
        /// 获取复杂型session abelp 2020/12/08 14:56
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetObject<T>(string key)
        {
            var value = _httpContextAccessor.HttpContext.Session.GetString(key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }

        /// <summary>
        /// 添加Session abelp 2020/12/08 14:59
        /// </summary>
        /// <param name="strSessionName">Session对象名称</param>
        /// <param name="strValue">Session值</param>
        public void Add(string strSessionName, string strValue)
        {
            _httpContextAccessor.HttpContext.Session.SetString(strSessionName, strValue);
        }

        /// <summary>
        /// 读取某个Session对象值 abelp 2020/12/08 15:05
        /// </summary>
        /// <param name="strSessionName">Session对象名称</param>
        /// <returns>Session对象值</returns>
        public object Get(string strSessionName)
        {
            if (_httpContextAccessor.HttpContext.Session == null
                || _httpContextAccessor.HttpContext.Session.GetString(strSessionName) == null)
            {
                return null;
            }
            else
            {
                return _httpContextAccessor.HttpContext.Session.GetString(strSessionName);
            }
        }

        /// <summary>
        /// 修改Session abelp 2020/12/08 15:09
        /// </summary>
        /// <param name="strSessionName"></param>
        /// <param name="objVal"></param>
        public void Update(string strSessionName, string objVal)
        {
            object obj = Get(strSessionName);
            if (obj != null)
            {
                Del(strSessionName);
            }
            Add(strSessionName, objVal);
        }

        /// <summary>
        /// 删除某个Session对象 abelp 2020/12/08 15:25
        /// </summary>
        /// <param name="strSessionName">Session对象名称</param>
        public void Del(string strSessionName)
        {
            _httpContextAccessor.HttpContext.Session.Remove(strSessionName);
        }


    }
}
